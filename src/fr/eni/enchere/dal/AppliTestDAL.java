package fr.eni.enchere.dal;

import java.sql.*;
import java.util.List;

public class AppliTestDAL {

	public static void main(String[] args) {
		Connection con = null;
		Statement stmt = null;

		//Charger le driver jdbc en mémoire
		try {
			//DriverManager.registerDriver(new SQLServerDriver());
			/*try {
				Class.forName(Settings.getProperty("driverjdbc"));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}*/

			String url = Settings.getProperty("url");
			String username = Settings.getProperty("user");
			String password = Settings.getProperty("pwd");

			//Obtenir une connexion
			con = DriverManager.getConnection(url, username, password);

			//Créer un statement
			stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery("select id, name from client");
			while(rs.next()){
				System.out.println(rs.getString("id") + " " + rs.getString("name"));
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException throwables) {
					throwables.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
